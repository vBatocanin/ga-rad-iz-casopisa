% !TEX encoding = UTF-8 Unicode

\documentclass[a4paper]{article}

\usepackage{color}
\usepackage{url}
\usepackage[T2A]{fontenc} % enable Cyrillic fonts
\usepackage[utf8]{inputenc} % make weird characters work
\usepackage{graphicx}

\usepackage[english,serbian]{babel}
%\usepackage[english,serbianc]{babel} %ukljuciti babel sa ovim opcijama, umesto gornjim, ukoliko se koristi cirilica

\usepackage[unicode]{hyperref}
\hypersetup{colorlinks,citecolor=green,filecolor=green,linkcolor=blue,urlcolor=blue}

%\newtheorem{primer}{Пример}[section] %ćirilični primer
\newtheorem{primer}{Primer}[section]

\usepackage{amssymb}
\usepackage{listings}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ 
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
  basicstyle=\scriptsize\ttfamily,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  firstnumber=1000,                % start line enumeration with line 1000
  frame=single,	                   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Python,                 % the language of the code
  morekeywords={*,...},            % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,	                   % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}

\begin{document}

\title{Problem izlivanja tečnosti iz poligona-ili-vađenje lopte iz poligona kotrljanjem \\ \small{Seminarski rad u okviru kursa\\Geometrijski algoritmi\\ Matematički fakultet}}

\author{Vladimir Batoćanin, 1068/2021}
\maketitle

\abstract{

Autori ovog rada obrađuju problem nalaženja broja rupa potrebnih da bi se proizvoljni poligon pun vode kompletno ispraznio isključivo rotacijom. Ovo postižu uvođenjem  dva ključna koncepta, od kojih je prvi koncept \textit{klopke}, koje predstavljaju delove poligona iz kog kapljice ne mogu dospeti u ostatka poligona. Drugi je koncept \textit{grafa dostižnosti}, koji nam govori za svako teme poligona koja temena su mu \textit{dostižna} u jednom potezu (jednim naginjanjem odnosno rotacijom).

Koristeći ove koncepte, autori konstruišu algoritam za nalaženje broja rupa potrebnih za pražnjenje dvodimenzionih poligona sa vremenskom složenošću $O(n^2logn)$. Nakon čega argumentuju zašto bi isti postupak imao polinomijalnu složenost prilikom primene na poliedrima.

Ovakav algoritam bi imao primenu prilikom konstrukcije poliedarskih kalupa koji se pune tečnim metalom \cite{filling} za proizvodnju mašinskih komponenti.

Rad je objavljen u februaru 2014. godine u časopisu ''Computational Geometry (volume 47)'' pod imenom ''Draining a polygon—or—rolling a ball out of a polygon''. Autori rada su: Greg Aloupis\footnotemark[1]{}, Jean Cardinal\footnotemark[1]{}, Sébastien Collette\footnotemark[1]{}, Ferran Hurtado\footnotemark[2]{}, Stefan Langerman\footnotemark[1]{}, Joseph O’Rourke \footnotemark[3]{} 
\footnotetext[1]{Université Libre de Bruxelles (ULB), CP212, Bld. du Triomphe, 1050 Brussels, Belgium}
\footnotetext[2]{Universitat Politècnica de Catalunya, Jordi Girona 1–3, E-08034 Barcelona, Spain}
\footnotetext[3]{Smith College, Northampton, MA 01063, USA}

Preliminarna verzija rada je objavljena u ''Proceedings of the 20th Canadian Conference on Computational Geometry'', Montreal, QC, Canada, August 2008.}
 

\tableofcontents

\newpage

\section{Uvod}
\label{sec:uvod}
\textbf{Inicijalni problema} Za dati poligon $P$ naći broj i lokaciju tačkastih rupa potrebnih da bi se iz poligona kompletno izlila tečnost. Temena poligona $v_i$ su numerisana suprotno od smera kazaljke na satu. \newline

Da bi načinili problem intuitivnijim, autori su napravili analogiju između vodenih kapljica i jedne jedine kuglice $\mathcal{B}$. Dakle problem se svodi na navigiranje kuglice kroz unutrašnjost poligona do tačkaste rupice.

Da bi konkretizovali način kretanja kuglice $\mathcal{B}$, uveli su dva modela kretanja: rotacioni i nagibni.

U rotacionom modelu poligon leži u $xy$ ravni, pravac vektora gravitacije je vertikalno na dole ($-y$ pravac). Kretanje u ovom modelu se realizuje ili menjanjem smera gravitacionog vektora ili rotacijom celog poligona za neki ugao oko imaginarne $z$-ose. Kuglica se od nekog temena $v_i$ pomera kada jedna od njenih susednih ivica $e_i=v_iv_{i+1}$ zaklapa sa vektorom gravitacije ugao manji od 90\textdegree.    

U nagibnom modelu se kretanje kuglice realizuje biranjem bilo kog vektora gravitacije $\vec{g}$. Neka se kuglica trenutno nalazi u temenu $v_i$, ukoliko vektor gravitacie $\vec{g}$ zaklapa ugao manji od 90{\textdegree} u negativnom smeru sa $u=v_{i+1}-v_i$ ili u pozitivnom smeru sa $ w=v_{i-1}-v_i$, krenuće duž odgovarajuće ivice. Međutim, ako se vektor $\vec{g}$ nalazi između vektora $u$ i $w$ ka unutrašnjosti poligona, kuglica će krenuti da pada u pravcu i smeru vektora gravitacije.

U oba modela, u slučaju da kuglica padne na neku ivicu pod tačno pravim uglom, kotrljaće se u proizvoljnom izabranom smeru (u ovom radu je to u smeru kazaljke na satu). U oba modela je takođe zabranjeno da se menja vektor gravitace dok se loptica pomera, odnosno dok nije u stacioniranom stanju.

\section{Klopke i specijalni poligoni}

Da bi autori postavili brojčana ograničenja za broj rupa potreban da se poligon isprazni, razmotrili su nekoliko različitih kategorija specijalnih poligona.

Pre svega su razmotreni poligoni za koje je potrebno $\Omega(n)$ rupa da bi se ispraznio. Osnovna ideja iza konstrukcije ovakvih poligona je da se kuglica zarobi u ciklus izmedju dve konveksne tačke, ovo može da se postigne korišćenjem 6 tačaka, konstruisanjem oblika koji liči na strelicu sa dva produžena kraka. Ovakav oblik je pogodan za rotacioni ali ne i nagibni model, s obzirom da koplje ima konveksnu tačku na svom vrhu, iz koje se nagibanjem vrlo lako može izaći. ovo se rešava odsecanjem vrha strelice u liniji sa produženim kracima.\newline

\textbf{Teorema} U rotacionom modelu postoje poligoni koji zahtevaju $ \lfloor n/12 \rfloor $ rupa da bi se ispraznio, gde je $n$ broj tačaka, dok u nagibnom modelu zahtevaju $ \lfloor n/28 \rfloor $ rupa.

Kao odgovor na ovu teoremu je moguće konstruisati ortogonalan poligon sa klopkama vrlo sličnim odsečenim strelicama iz prošlog primera. Ako bismo želeli da konstruišemo poligon sa $k$ ortogonalnih klopki, on bi ukupno morao da ima $n=28k+4$ temena. Ovakav poligon zahteva $k$ rupa da bi se ispraznio. 

Postoje i poligoni koji liče na zvezdu sa proizvoljnim brojem krakova koji koriste blage nagibe u bazama svojih krakova da bi zarobili kuglicu u ciklusu, gde može samo da upadne u krakove određenog modula. Najjednostavniji oblik ovoga bi bila zvezda za koju je potrebno 2 rupe jer ne nemoguće dospeti iz parnih krakova u neparne krakove.\newline

Moguće je doći do gornjeg ograničenja broja potrebnih rupa tako što se sva konveksna temena svrstaju u kategorije na osnovu toga u kom kvadrantu im se nalazi većina unutrašnjosti poligona. Na osnovu osobina ortogonalnih poligona i minimuma učestalosti svake kategorije konveksnih temena se može zaključiti da je opšta gornja granica za potreban broj rupa u poligonu $\lfloor(n+4)/8\rfloor$.

\section{Algoritam minimalnog broja rupa}

Za realizaciju ovog algoritma je potrebno prvo generisati nekoliko pomoćnih struktura:
\begin{itemize}
    \item Graf dostižnosti $G$
    \item Graf jakih komponenti povezanosti $G*$
    \item Gravitacioni dijagram
\end{itemize}

Gravitacioni dijagram je pomočna struktura koja nam ubrzava proces traženja suseda svakog čvora na osnovu proizvoljnog gravitacionog vektora, i time ubrzava i proces kreiranja narednih pomoćnih struktura.

Neka je $G$ \textit{graf dostižnosti} čiji čvorovi odgovaraju konveksnim temenima poligona $P$. Dva čvora $v_i$ i $v_j$ su povezana ukoliko kuglica $B$ može se otkotrlja od jedne do druge tačke u jednom potezu (rotacijom ili naginjanjem). Za svaku granu grafa $(v_i,v_j) \in G$ čuvamo skup puteva koji vode od jednog čvora do drugog

Neka su $C_1,C_2,...$ jako povezane komponente grafa $G$. Svaku komponentu $C_k$ pridružimo jednom čvoru $c_k$ grafa \textit{jakih komponenti} $G*$. Sve grane koje povezuju komponente u originalnom grafu $G$ se prenose na graf $G*$.

Ovaj graf je koristan jer je broj rupa potreban za pražnjenje poligona P isti kao broj čvorova u grafu $G*$ koji nemaju izlaznu granu. Što znači da imamo jedan deo problema rešen, sledeće je potrebno naći lokaciju dotičnih rupa.


Konačni algoritam za nalaženje minimalnog broja rupa u poligonu se sastoji iz sledećih koraka:
\begin{enumerate}
    \item Konstruisanje $G$ u $O(n^2logn)$ koraka
    \item Nalaženje jako povezanih komponenti u $G$ u $O(|G|)=O(n^2)$ koraka \cite{intro}
    \item Konstruisanje grafa jakih komponenti $G*$ u $O(|G)$ koraka
    \item Nalaženje ponornih čvorova u $G*$ u $O(|G)$ koraka
    \item Biranje jednog konveksnog temena u svakom ponoru da sadrži rupu
\end{enumerate}

Konstrukcija pomoćnog grafa $G_t$ u slučaju da se koristi samo model naginjanja može da se ubrza tako što se sam poligon $P$ pretprocesira za bacanje zraka iz svakog temena\cite{ray}, na osnovu čega se generišu gravitacioni dijagrami koji se onda mogu koristiti za brz prolazak kroz sve moguće puteve između dva čvora\cite{visible}. Kompleksnost optimizovanog generisanja $G_t$ je $O(n^2logn)$, s toga je onda i ceo algoritam nalaska lokacija rupa u poligonu kompleksnosti $O(n^2logn)$. \newpage

Postupak nalaženja rupa se može uopštiti i na oblik više dimenzije, na primer u trodimenzionom poliedru postupak je vrlo sličan, sa par ključnih razlika:
\begin{enumerate}
    \item Domen gravitacionog vektora se proširuje da može da pripada ravnima svih incidentnih lica trenutno posmatrane tačke
    \item Umesto Grafa vidljivosti, za pravljenje gravitacionih dijagrama koristi se trodimenziona sfera čiji je centar u temenu koje trenutno posmatramo
    \item Gravitacioni dijagrami moraju da se prošire na dekartov proizvod svih mogućih preseka sfere sa datom ivicom sa svim mogućim polaznim gravitacionim vektorima (što topološki predsavlja cilindar)
\end{enumerate}

Ovaj algoritam nije savršen, i u idealnom slučaju bi adresirao i dimenzije kuglice (odnosno kapljice vode), kao i njena svojstva. Na primer prilikom spuštanja na neko teme da se podeli na više delova. Ovakav pristup bi doduše zahtevao daleko kompleksniji model.

\addcontentsline{toc}{section}{Literatura}
\appendix
\bibliography{references} 
\bibliographystyle{plain}



\end{document}
